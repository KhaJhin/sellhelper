﻿namespace SellHelper1._0
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.OK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.ChoseCB = new System.Windows.Forms.ComboBox();
            this.UploadEX = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.навигацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.naviMain = new System.Windows.Forms.ToolStripMenuItem();
            this.naviChange = new System.Windows.Forms.ToolStripMenuItem();
            this.naviRed = new System.Windows.Forms.ToolStripMenuItem();
            this.naviExit = new System.Windows.Forms.ToolStripMenuItem();
            this.OFP = new System.Windows.Forms.OpenFileDialog();
            this.Chosen = new System.Windows.Forms.Label();
            this.SaveExcel = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 82);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(361, 225);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // OK
            // 
            this.OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OK.BackColor = System.Drawing.Color.Transparent;
            this.OK.Location = new System.Drawing.Point(305, 18);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(50, 21);
            this.OK.TabIndex = 1;
            this.OK.Text = "ОК";
            this.OK.UseVisualStyleBackColor = false;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.ChoseCB);
            this.groupBox1.Controls.Add(this.OK);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(361, 49);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выберите таблицу";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.Location = new System.Drawing.Point(195, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(104, 20);
            this.textBox1.TabIndex = 8;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // ChoseCB
            // 
            this.ChoseCB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChoseCB.FormattingEnabled = true;
            this.ChoseCB.Items.AddRange(new object[] {
            "HDDs",
            "Cases",
            "BPs",
            "Procs",
            "VideoCards",
            "Motherboards",
            "DDRs",
            "SDDs",
            "Users",
            "Stores",
            "Compls",
            "Supltypes",
            "History",
            "Defects"});
            this.ChoseCB.Location = new System.Drawing.Point(6, 18);
            this.ChoseCB.Name = "ChoseCB";
            this.ChoseCB.Size = new System.Drawing.Size(183, 21);
            this.ChoseCB.TabIndex = 2;
            // 
            // UploadEX
            // 
            this.UploadEX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.UploadEX.BackColor = System.Drawing.Color.Transparent;
            this.UploadEX.Location = new System.Drawing.Point(12, 313);
            this.UploadEX.Name = "UploadEX";
            this.UploadEX.Size = new System.Drawing.Size(100, 23);
            this.UploadEX.TabIndex = 3;
            this.UploadEX.Text = "Загрузить Excel";
            this.UploadEX.UseVisualStyleBackColor = false;
            this.UploadEX.Click += new System.EventHandler(this.UploadEX_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Location = new System.Drawing.Point(235, 313);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Сохранить в базу ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.навигацияToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(385, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // навигацияToolStripMenuItem
            // 
            this.навигацияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.naviMain,
            this.naviChange,
            this.naviRed,
            this.naviExit});
            this.навигацияToolStripMenuItem.Name = "навигацияToolStripMenuItem";
            this.навигацияToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.навигацияToolStripMenuItem.Text = "Навигация";
            // 
            // naviMain
            // 
            this.naviMain.Name = "naviMain";
            this.naviMain.Size = new System.Drawing.Size(200, 22);
            this.naviMain.Text = "Главная";
            this.naviMain.Click += new System.EventHandler(this.главнаяToolStripMenuItem_Click);
            // 
            // naviChange
            // 
            this.naviChange.Name = "naviChange";
            this.naviChange.Size = new System.Drawing.Size(200, 22);
            this.naviChange.Text = "Сменить пользователя";
            this.naviChange.Click += new System.EventHandler(this.сменитьПользователяToolStripMenuItem_Click);
            // 
            // naviRed
            // 
            this.naviRed.Name = "naviRed";
            this.naviRed.Size = new System.Drawing.Size(200, 22);
            this.naviRed.Text = "Редактировать базу";
            // 
            // naviExit
            // 
            this.naviExit.Name = "naviExit";
            this.naviExit.Size = new System.Drawing.Size(200, 22);
            this.naviExit.Text = "Выйти";
            this.naviExit.Click += new System.EventHandler(this.naviExit_Click);
            // 
            // OFP
            // 
            this.OFP.FileName = "openFileDialog1";
            // 
            // Chosen
            // 
            this.Chosen.AutoSize = true;
            this.Chosen.Location = new System.Drawing.Point(9, 339);
            this.Chosen.Name = "Chosen";
            this.Chosen.Size = new System.Drawing.Size(0, 13);
            this.Chosen.TabIndex = 6;
            // 
            // SaveExcel
            // 
            this.SaveExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveExcel.BackColor = System.Drawing.Color.Transparent;
            this.SaveExcel.Location = new System.Drawing.Point(118, 313);
            this.SaveExcel.Name = "SaveExcel";
            this.SaveExcel.Size = new System.Drawing.Size(111, 23);
            this.SaveExcel.TabIndex = 7;
            this.SaveExcel.Text = "Сохранить в Excel";
            this.SaveExcel.UseVisualStyleBackColor = false;
            this.SaveExcel.Click += new System.EventHandler(this.SaveExcel_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.Azure;
            this.progressBar1.Location = new System.Drawing.Point(235, 340);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(137, 12);
            this.progressBar1.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(173, 339);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Прогресс";
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(385, 361);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.SaveExcel);
            this.Controls.Add(this.Chosen);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.UploadEX);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "AdminForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Администратор";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ChoseCB;
        private System.Windows.Forms.Button UploadEX;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem навигацияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem naviMain;
        private System.Windows.Forms.ToolStripMenuItem naviChange;
        private System.Windows.Forms.ToolStripMenuItem naviRed;
        private System.Windows.Forms.ToolStripMenuItem naviExit;
        private System.Windows.Forms.OpenFileDialog OFP;
        private System.Windows.Forms.Label Chosen;
        private System.Windows.Forms.Button SaveExcel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
    }
}