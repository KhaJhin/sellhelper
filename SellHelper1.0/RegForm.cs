﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SellHelper1._0
{
    public partial class RegForm : Form
    {
        public RegForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }

        private void OKBTN_Click(object sender, EventArgs e)
        {
            try
            {
                if (gl.status == false)

                {
                    DialogResult result = MessageBox.Show("Нет подключения к базе! Подключиться?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                    {
                        if (gl.connect == null)
                            gl.connect = new connecttobase();
                        this.Hide();
                        gl.connect.Show();
                    }
                }
                else
                {
                    connections.server(gl.username, gl.pass);
                    string user = loginTB.Text;
                    string log = "";
                    string selectLogin = "select Login from users where Login ='" + user + "';";
                    MySqlCommand getPass = new MySqlCommand(selectLogin, gl.Connect);
                    MySqlDataReader myReader;
                    myReader = getPass.ExecuteReader();

                    while (myReader.Read())
                    {
                        log = myReader[0].ToString();
                    }
                    myReader.Close();
                    string currlogin = loginTB.Text;
                   
                    if (currlogin == log)
                    {
                        MessageBox.Show("Регистрация не удалась. Такой Логин существует", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        loginTB.Text = "";
                        passTB.Text ="";
                    }
                    else
                    {
                            string reg = "insert into users(Login,Password,Firstname,Lastname,Post) values('"+loginTB.Text+"','"+passTB.Text+"','"+nTB.Text+"','"+fTB.Text+"','null');";
                            MySqlCommand coma = new MySqlCommand(reg, gl.Connect);
                            coma.ExecuteNonQuery();
                            MessageBox.Show("Регистрация прошла успешно", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Hide();
                            gl.loginform.Show();
                    }
                }
            }
            catch
            {
                if (loginTB.Text == "" || passTB.Text == "" || fTB.Text == "" || nTB.Text == "")
                MessageBox.Show("Регистрация не удалась. Не все данные введены","Warning!",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            
        }

        private void RegForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
