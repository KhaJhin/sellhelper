﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SellHelper1._0
{
    public partial class Defect : Form
    {
        public Defect()
        {
            InitializeComponent();
            MinimizeBox = false;
            MaximizeBox = false;
            
        }

        string coast;
        private void OKBTN_Click(object sender, EventArgs e)
        {
            connections.server(gl.username, gl.pass);

            if (defCB.Text == "")
            {
                MessageBox.Show("Выберите коэффициент дефекта!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {

                //Получаем стоимость с дефектом
                MySqlDataReader myReader;
                string getCoast = "select Coast from " + gl.CurrentTable + " where id ='" + gl.CurrentCellID + "';";
                MySqlCommand coms = new MySqlCommand(getCoast, gl.Connect);
                myReader = coms.ExecuteReader();
                while (myReader.Read())
                {
                    coast = myReader[0].ToString();
                }
                myReader.Close();
                float defcoef = Convert.ToSingle(defCB.Text) / 100;            
                float coastwithdefect = 0;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                coastwithdefect = Convert.ToSingle(coast) * defcoef;
                //insert запрос 
                string insertquery = "insert into bufers (id_compl , Supl_Id ,Supltype, Suplname,Defect,CoastWithDefect,Commentaries, Status) " +
                    "values ('"+gl.idCompl+"','"+gl.CurrentCellID+"','"+gl.CurrentTable+"','"+gl.suplname+"','"+defCB.Text+"','"+coastwithdefect+"','"+comboBox1.Text+"','Скупка');";
                MySqlCommand com = new MySqlCommand(insertquery, gl.Connect);
                com.ExecuteNonQuery();
                MessageBox.Show("Успешно", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                gl.def.Hide();
                gl.sellform.Activate();
                
            }


        }

        private void Defect_Activated(object sender, EventArgs e)
        {
            defCB.Text = "";
            comboBox1.Text = "";
            connections.server(gl.username, gl.pass);
            //заполнение датасета
            string query = "select distinct coef from defects where supltype = '"+gl.CurrentTable+"';";
            DataSet dataset = new DataSet();
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, gl.Connect);
            adapter.Fill(dataset);
            //отображение в comboBox1
            defCB.DataSource = dataset.Tables[0];
            defCB.DisplayMember = "coef";
            
        }

        private void defCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            connections.server(gl.username, gl.pass);
            //заполнение датасета
            string query = "select description from defects where supltype = '" + gl.CurrentTable + "' AND coef ='"+defCB.Text+"';";
            DataSet dataset = new DataSet();
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, gl.Connect);
            adapter.Fill(dataset);
            //отображение в comboBox1
            comboBox1.DataSource = dataset.Tables[0];
            comboBox1.DisplayMember = "description";
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
          
        }
    }
}
