﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;

namespace SellHelper1._0
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();
            naviRed.Enabled = false;
        }

        public static string filename;
        public static string fileText;
        public static DataSet ds;
        public static MySqlDataAdapter adapter, adapter2;
        public static MySqlCommandBuilder commandBuilder;
        public static DataSet dataSet, dataSet2;
        public static bool trigger = false;
        public static DataTable tb;
      

        private void главнаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            gl.sellform.Show();
        }

        private void сменитьПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            gl.loginform.Show();
        }

        private void fillDataSet()
        {
            connections.server(gl.username, gl.pass);
            adapter = new MySqlDataAdapter("select * from " + ChoseCB.Text, gl.Connect);
            dataSet = new DataSet();
            adapter.Fill(dataSet);
        }

        private void fillGrid()
        {
            dataGridView1.DataSource = dataSet.Tables[0];
        }

        private void updateGrid()
        {
            dataGridView1.Columns.Clear();
           fillDataSet();
            fillGrid();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            Chosen.Text = "";
            trigger = false;
            updateGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           connections.server(gl.username, gl.pass);
            if (trigger == false)
            {
                try
                {
                    commandBuilder = new MySqlCommandBuilder(adapter);
                    adapter.Update(dataSet);
                    MessageBox.Show("Изменения в базе данных выполнены!", "Уведомление о результатах", MessageBoxButtons.OK);
                }
                catch
                {
                    MessageBox.Show("Внимание! Не выбрана таблица", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                progressBar1.Maximum = dataGridView1.RowCount;
                progressBar1.Value = 0;
                string truncate = "truncate " + ChoseCB.Text+";";
                MySqlCommand com1 = new MySqlCommand(truncate, gl.Connect);
                com1.ExecuteNonQuery();
                string insrt,values="";
                for (int i = 0; i < dataGridView1.RowCount-1; i++)
                {   for (int j = 0; j < dataGridView1.ColumnCount; j++)
                        
                    {                        
                            values += "'" + dataGridView1.Rows[i].Cells[j].Value.ToString() + "',";                                    
                    }
                    progressBar1.Value++;
                    // индекс последнего символа
                    int ind = values.Length - 1;
                    // вырезаем последний символ 
                    values = values.Remove(ind);
                    insrt = "insert into " + ChoseCB.Text + " values ( " + values + " );";
                    MySqlCommand com2 = new MySqlCommand(insrt, gl.Connect);
                    com2.ExecuteNonQuery();
                    values = "";
                }
                MessageBox.Show("Изменения в базе данных выполнены!", "Уведомление о результатах", MessageBoxButtons.OK);
                progressBar1.Value = 0;
            }
        }

        private void UploadEX_Click(object sender, EventArgs e)
        {
            trigger = true;
            dataGridView1.Columns.Clear();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = "*.xlsx;*.xls";
            ofd.Filter = "Excel 2007(*.xlsx)|*.xlsx|Excel 2003(*.xls)|*.xls";
            ofd.Title = "Выберите документ для загрузки данных";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                filename = ofd.FileName;
                fileText = Path.GetFileNameWithoutExtension(filename);

                String constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                ofd.FileName +
                                ";Extended Properties='Excel 12.0 XML;HDR=YES;';";

                System.Data.OleDb.OleDbConnection con =
                    new System.Data.OleDb.OleDbConnection(constr);
                con.Open();
                             ds = new DataSet();
                DataTable schemaTable = con.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables,
                    new object[] { null, null, null, "TABLE" });

                string sheet1 = (string)schemaTable.Rows[0].ItemArray[2];
                string select = String.Format("SELECT * FROM [{0}]", sheet1);
                System.Data.OleDb.OleDbDataAdapter ad =
                    new System.Data.OleDb.OleDbDataAdapter(select, con);
                ad.Fill(ds);
                tb = ds.Tables[0];
                con.Close();
                dataGridView1.DataSource = tb;
                con.Close();
                 ChoseCB.Text = fileText + "s";
                Chosen.Text = "Выбран файл: " + fileText;
                MessageBox.Show("Файл открыт");
            }
            else
            {
                MessageBox.Show("Вы не выбрали файл для открытия",
                        "Загрузка данных...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveExcel_Click(object sender, EventArgs e)
        {
            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add();
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            try
            {

                worksheet = workbook.ActiveSheet;
                int cellRowIndex = 1;
                int cellColumnIndex = 1;

                //Loop through each row and read value from each column. 
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dataGridView1.Columns[j].HeaderText;
                        }
                        else
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }

                workbook.SaveAs(filename);
                MessageBox.Show("Export Successful");
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void naviExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AdminForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
             MessageBox.Show("Входное значение имело неверный формат", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //поиск
            connections.server(gl.username, gl.pass);
            gl.CurrentTable = ChoseCB.Text;
            string searching;

            if (textBox1.Text == "")
            {
                searching = "select * from " + gl.CurrentTable + ";";
            }
            else
            {
                searching = "select * from " + gl.CurrentTable + " where " + gl.colname + " like '%" + textBox1.Text + "%';";
            }
            adapter2 = new MySqlDataAdapter(searching, gl.Connect);
            dataSet2 = new DataSet();
            adapter2.Fill(dataSet2);
            dataGridView1.DataSource = dataSet2.Tables[0];
            dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[gl.col];

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                gl.col = e.ColumnIndex;
                gl.colname = "";
                gl.colname = dataGridView1.Columns[gl.col].HeaderText;
            }
            catch
            {
                gl.col = e.ColumnIndex;
                gl.colname = "";
                gl.colname = dataGridView1.Columns[gl.col + 1].HeaderText;
            }
        }
    }
}