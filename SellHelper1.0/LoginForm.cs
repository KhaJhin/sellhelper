﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace SellHelper1._0
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;                                 
        }
       
        private void regBTN_Click(object sender, EventArgs e)
        {
            if (gl.regform == null)
                gl.regform = new RegForm();
                this.Hide();
            gl.regform.Show();
            
        }

        private void entBTN_Click(object sender, EventArgs e)
        {

            if (gl.status == false)

            {
                DialogResult result = MessageBox.Show("Нет подключения к базе! Подключиться?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (gl.connect == null)
                        gl.connect = new connecttobase();
                    this.Hide();
                    gl.connect.Show();
                }
            }
            else
            {
                try
                {
                    if (passTB.Text == "" || loginTB.Text == "")
                    {
                        MessageBox.Show("Не все данные введены", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        gl.usser = loginTB.Text;
                        string pass = passTB.Text;
                        string curpass = "";
                        string selectpassword = "select Password from users where Login ='" + gl.usser + "';";
                        MySqlCommand getPass = new MySqlCommand(selectpassword, gl.Connect);
                        MySqlDataReader myReader;
                        myReader = getPass.ExecuteReader();

                        while (myReader.Read())
                        {
                            curpass = myReader[0].ToString();
                        }
                        myReader.Close();

                        if (curpass == pass)
                        {
                            MessageBox.Show("Вход выполнен. Вы вошли как " + gl.usser, "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (gl.sellform == null)
                                gl.sellform = new Sellform();
                            this.Hide();
                            gl.sellform.Show();
                        }
                        else
                        {
                            MessageBox.Show("Вход не выполнен, проверьте введенные данные");
                        }

                    }
                }
                catch
                {
                    MessageBox.Show("Вход не выполнен, проверьте введенные данные");
                }
            }
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

       

        private void button2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                button2.BackgroundImage = Properties.Resources.show;
                passTB.UseSystemPasswordChar = false;
            }
        }   

        private void button2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                button2.BackgroundImage = Properties.Resources.multimedia;
                passTB.UseSystemPasswordChar = true;
            }
        }
    }
}
