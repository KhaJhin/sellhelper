﻿namespace SellHelper1._0
{
    partial class Sellform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ChoseCB = new System.Windows.Forms.ComboBox();
            this.OK = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DoneBTN = new System.Windows.Forms.Button();
            this.BuyRB = new System.Windows.Forms.RadioButton();
            this.SellRB = new System.Windows.Forms.RadioButton();
            this.label = new System.Windows.Forms.Label();
            this.userlabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.navi = new System.Windows.Forms.ToolStripMenuItem();
            this.naviMain = new System.Windows.Forms.ToolStripMenuItem();
            this.naviChangeUser = new System.Windows.Forms.ToolStripMenuItem();
            this.naviBase = new System.Windows.Forms.ToolStripMenuItem();
            this.naviExit = new System.Windows.Forms.ToolStripMenuItem();
            this.ComplID = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.ChoseCB);
            this.groupBox1.Controls.Add(this.OK);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(834, 49);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выберите таблицу";
            // 
            // ChoseCB
            // 
            this.ChoseCB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChoseCB.FormattingEnabled = true;
            this.ChoseCB.Items.AddRange(new object[] {
            "HDDs",
            "Cases",
            "BPs",
            "Procs",
            "VideoCards",
            "Motherboards",
            "DDRs",
            "SDDs",
            "Stores"});
            this.ChoseCB.Location = new System.Drawing.Point(6, 19);
            this.ChoseCB.Name = "ChoseCB";
            this.ChoseCB.Size = new System.Drawing.Size(635, 21);
            this.ChoseCB.TabIndex = 2;
            // 
            // OK
            // 
            this.OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OK.BackColor = System.Drawing.Color.Transparent;
            this.OK.Location = new System.Drawing.Point(752, 18);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(50, 21);
            this.OK.TabIndex = 1;
            this.OK.Text = "ОК";
            this.OK.UseVisualStyleBackColor = false;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(647, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 21);
            this.button1.TabIndex = 43;
            this.button1.Text = "Создать сборку";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 82);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(834, 104);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.Location = new System.Drawing.Point(12, 497);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(116, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(15, 481);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Поиск";
            // 
            // DoneBTN
            // 
            this.DoneBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.DoneBTN.Location = new System.Drawing.Point(766, 508);
            this.DoneBTN.Name = "DoneBTN";
            this.DoneBTN.Size = new System.Drawing.Size(80, 23);
            this.DoneBTN.TabIndex = 8;
            this.DoneBTN.Text = "Завершить";
            this.DoneBTN.UseVisualStyleBackColor = true;
            this.DoneBTN.Click += new System.EventHandler(this.DoneBTN_Click);
            // 
            // BuyRB
            // 
            this.BuyRB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BuyRB.AutoSize = true;
            this.BuyRB.Checked = true;
            this.BuyRB.Cursor = System.Windows.Forms.Cursors.Default;
            this.BuyRB.Location = new System.Drawing.Point(134, 488);
            this.BuyRB.Name = "BuyRB";
            this.BuyRB.Size = new System.Drawing.Size(61, 17);
            this.BuyRB.TabIndex = 9;
            this.BuyRB.TabStop = true;
            this.BuyRB.Text = "Скупка";
            this.BuyRB.UseVisualStyleBackColor = true;
            // 
            // SellRB
            // 
            this.SellRB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SellRB.AutoSize = true;
            this.SellRB.Location = new System.Drawing.Point(134, 511);
            this.SellRB.Name = "SellRB";
            this.SellRB.Size = new System.Drawing.Size(71, 17);
            this.SellRB.TabIndex = 10;
            this.SellRB.Text = "Продажа";
            this.SellRB.UseVisualStyleBackColor = true;
            this.SellRB.CheckedChanged += new System.EventHandler(this.SellRB_CheckedChanged);
            // 
            // label
            // 
            this.label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(12, 522);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(97, 13);
            this.label.TabIndex = 11;
            this.label.Text = "Текущая сборка: ";
            // 
            // userlabel
            // 
            this.userlabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.userlabel.AutoSize = true;
            this.userlabel.BackColor = System.Drawing.SystemColors.Window;
            this.userlabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.userlabel.Location = new System.Drawing.Point(632, 6);
            this.userlabel.Name = "userlabel";
            this.userlabel.Size = new System.Drawing.Size(86, 13);
            this.userlabel.TabIndex = 12;
            this.userlabel.Text = "Пользователь: ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Window;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.navi});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(858, 24);
            this.menuStrip1.TabIndex = 42;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // navi
            // 
            this.navi.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.naviMain,
            this.naviChangeUser,
            this.naviBase,
            this.naviExit});
            this.navi.Name = "navi";
            this.navi.Size = new System.Drawing.Size(78, 20);
            this.navi.Text = "Навигация";
            // 
            // naviMain
            // 
            this.naviMain.Name = "naviMain";
            this.naviMain.Size = new System.Drawing.Size(200, 22);
            this.naviMain.Text = "Главная";
            // 
            // naviChangeUser
            // 
            this.naviChangeUser.Name = "naviChangeUser";
            this.naviChangeUser.Size = new System.Drawing.Size(200, 22);
            this.naviChangeUser.Text = "Сменить пользователя";
            this.naviChangeUser.Click += new System.EventHandler(this.naviChangeUser_Click);
            // 
            // naviBase
            // 
            this.naviBase.Name = "naviBase";
            this.naviBase.Size = new System.Drawing.Size(200, 22);
            this.naviBase.Text = "Редактировать базу";
            this.naviBase.Click += new System.EventHandler(this.naviBase_Click);
            // 
            // naviExit
            // 
            this.naviExit.Name = "naviExit";
            this.naviExit.Size = new System.Drawing.Size(200, 22);
            this.naviExit.Text = "Выйти";
            this.naviExit.Click += new System.EventHandler(this.naviExit_Click);
            // 
            // ComplID
            // 
            this.ComplID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ComplID.AutoSize = true;
            this.ComplID.Location = new System.Drawing.Point(105, 522);
            this.ComplID.Name = "ComplID";
            this.ComplID.Size = new System.Drawing.Size(0, 13);
            this.ComplID.TabIndex = 44;
            // 
            // dataGridView2
            // 
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(12, 205);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(834, 273);
            this.dataGridView2.TabIndex = 45;
            this.dataGridView2.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView2_DataError);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(14, 189);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "Текущая сборка";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(661, 508);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 23);
            this.button2.TabIndex = 47;
            this.button2.Text = "Редактировать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Sellform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(858, 550);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.ComplID);
            this.Controls.Add(this.userlabel);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label);
            this.Controls.Add(this.SellRB);
            this.Controls.Add(this.BuyRB);
            this.Controls.Add(this.DoneBTN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Sellform";
            this.Text = "SellHelper 1.0";
            this.Activated += new System.EventHandler(this.Sellform_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Sellform_FormClosed);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ChoseCB;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DoneBTN;
        private System.Windows.Forms.RadioButton BuyRB;
        private System.Windows.Forms.RadioButton SellRB;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label userlabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem navi;
        private System.Windows.Forms.ToolStripMenuItem naviMain;
        private System.Windows.Forms.ToolStripMenuItem naviChangeUser;
        private System.Windows.Forms.ToolStripMenuItem naviBase;
        private System.Windows.Forms.ToolStripMenuItem naviExit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label ComplID;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
    }
}