﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SellHelper1._0
{
    public partial class connecttobase : Form
    {
        public connecttobase()
        {
            InitializeComponent();
            MinimizeBox = false;
            MaximizeBox = false;
        }

        private void OkBTN_Click(object sender, EventArgs e)
        {
            gl.username = textBox1.Text;
            gl.pass = passTB.Text;

            try
            {
                connections.server(gl.username, gl.pass);
                gl.status = true;

                if (gl.loginform == null)
                    gl.loginform = new LoginForm();
                gl.loginform.Show();
                this.Hide();
                MessageBox.Show("Соединение установлено", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Соединение не установлено, проверьте ввденные данные","Ошибка",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
        }

        private void connecttobase_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button2_MouseUp(object sender, MouseEventArgs e)
        {
             if (e.Button == MouseButtons.Left)
                    {
                        button2.BackgroundImage = Properties.Resources.multimedia;
                        passTB.UseSystemPasswordChar = true;
                    }
        }

        private void button2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                button2.BackgroundImage = Properties.Resources.show;
                passTB.UseSystemPasswordChar = false;
            }
        }
    }
}
