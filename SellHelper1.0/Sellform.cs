﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.IO;

namespace SellHelper1._0
{
    public partial class Sellform : Form
    {
        public Sellform()
        {
            InitializeComponent();
            GetIDCompl();
            userlabel.Text += gl.usser;
            naviMain.Enabled = false;       
        }

        //Объявление переменных
        public static MySqlDataAdapter adapter;
        public static MySqlDataAdapter adapter1, adapter2;
        public static MySqlCommandBuilder commandBuilder;
        public static DataSet dataSet;
        public static DataSet dataSet1, dataSet2;
        public static string colname;
        
        //Получение ID сборки
        private void GetIDCompl()
        {
            connections.server(gl.username, gl.pass);
            MySqlDataReader myReader;
            string getID = " SELECT Id FROM Compls ORDER BY Id DESC LIMIT 1;";
            MySqlCommand comsrch = new MySqlCommand(getID, gl.Connect);
            myReader = comsrch.ExecuteReader();
           while (myReader.Read())
            {
                gl.idCompl = myReader[0].ToString();
            }
            myReader.Close();
            ComplID.Text = gl.idCompl;
        }

        //Заполнение DataSet
        private void fillDataSet()
        {
            if (ChoseCB.Text == "Stores")
            {

                connections.server(gl.username, gl.pass);
                adapter = new MySqlDataAdapter(@"select id as ID,
                                                 id_compl as  ID_Сборки,
                                                 Supl_id as ID_Оборудования,
                                                 Supltype as Тип_Оборудования,
                                                 Suplname as Наименование,
                                                 Defect as Дефект,
                                                 CoastWithDefect as Цена_с_дефектом,
                                                 Commentaries as Комментарий
                                                 from stores", gl.Connect);
                dataSet = new DataSet();
                adapter.Fill(dataSet);
            }
            else
            {
                connections.server(gl.username, gl.pass);
                adapter = new MySqlDataAdapter("select * from " + ChoseCB.Text, gl.Connect);
                dataSet = new DataSet();
                adapter.Fill(dataSet);
            }
        }

        //Заполнение DataGrid
        private void fillGrid()
        {
            dataGridView1.DataSource = dataSet.Tables[0];
        }

        //Обновление Datagrid
        private void updateGrid()
        {
            dataGridView1.Columns.Clear();
            fillDataSet();
            fillGrid();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            updateGrid();
            dataGridView1.ReadOnly = true;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Получение глобаьных данных
                gl.CurrentCellID = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
                gl.CurrentTable = ChoseCB.Text;
                gl.suplname = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();
            
            if (BuyRB.Checked == true)
            {
                if (gl.def == null)
                    gl.def = new Defect();
                gl.def.Show();
                gl.def.Activate();
            }
           else
            {
                // заполнение буфера 
                connections.server(gl.username, gl.pass);
                string insertintobufer = "insert into bufers (Id_compl,Supl_Id,Supltype,Suplname,Defect,Coastwithdefect,Commentaries, id_stores) select Id_compl,Supl_Id,Supltype,Suplname,Defect,Coastwithdefect,Commentaries, Id from sellhelper.stores where id = '"+gl.CurrentCellID+"';";
                MySqlCommand com = new MySqlCommand(insertintobufer, gl.Connect);
                com.ExecuteNonQuery();
                //вывод буфера в грид 
                adapter1 = new MySqlDataAdapter(@"select id as ID,
                                                 id_compl as  ID_Сборки,
                                                 Supl_id as ID_Оборудования,
                                                 Supltype as Тип_Оборудования,
                                                 Suplname as Наименование,
                                                 Defect as Дефект,
                                                 CoastWithDefect as Цена_с_дефектом,
                                                 Commentaries as Комментарий
                                                 from bufers " , gl.Connect);
                dataSet1 = new DataSet();
                adapter1.Fill(dataSet1);
                dataGridView2.DataSource = dataSet1.Tables[0];
            }         
        }

        //поиск
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            connections.server(gl.username, gl.pass);
            gl.CurrentTable = ChoseCB.Text;
            string searching;
            
            if (textBox1.Text == "")
            {
                searching = "select * from " + gl.CurrentTable + ";";
            }
            else
            {
                searching = "select * from " + gl.CurrentTable + " where " + gl.colname + " like '%" + textBox1.Text + "%';";
            }
            adapter2 = new MySqlDataAdapter(searching, gl.Connect);
            dataSet2 = new DataSet();
            adapter2.Fill(dataSet2);
            dataGridView1.DataSource = dataSet2.Tables[0];
            dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[gl.col];
        }

       
        private void naviExit_Click(object sender, EventArgs e)
        {
           Application.Exit();
        }

        // Редактирование базы
        string getpost;
        private void naviBase_Click(object sender, EventArgs e)
        {
            //Проверка должности
            MySqlDataReader myReader;
            string getpostquery = " select post from users where login = '"+gl.usser+"';";
            MySqlCommand comsrch = new MySqlCommand(getpostquery, gl.Connect);
            myReader = comsrch.ExecuteReader();
            while (myReader.Read())
            {
                getpost = myReader[0].ToString();
            }
            myReader.Close();
            if (getpost == "Администратор" || getpost == "superuser")
            {
                if (gl.adminform == null)
                    gl.adminform = new AdminForm();
                this.Hide();
                gl.adminform.Show();
            }
            else
            {
               DialogResult result = MessageBox.Show("Недостаточно прав! Обратитесь к администратору! Сменить пользователя?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (DialogResult.Yes == result)
                {
                    gl.loginform.Show();
                }

            }
        }

       //Сменить пользователя
        private void naviChangeUser_Click(object sender, EventArgs e)
        {
            this.Hide();
            gl.loginform.Show();
        }

        //Создание сборки
           
        private void button1_Click(object sender, EventArgs e)
        {
            if (gl.compl == null)
                gl.compl = new Compls();
            gl.compl.Show();

        }
        // функция для замены меток
        private void ReplaceStub(string stubToReplace, string text, Word.Document worldDocument)
        {
            var range = worldDocument.Content;
            range.Find.ClearFormatting();
            
            object wdReplaceAll = Word.WdReplace.wdReplaceAll;
            range.Find.Execute(FindText: stubToReplace, Wrap: Word.WdFindWrap.wdFindContinue);
            //range.Find.Execute(FindText: stubToReplace, ReplaceWith: text, Replace: wdReplaceAll, Wrap: Word.WdFindWrap.wdFindContinue);
            if (range.Find.Found == true)
                range.Text = text;
        }

        //Взаимодействие с таблицей stores
        private void DoneBTN_Click(object sender, EventArgs e)
        {
            //добавление записей
            connections.server(gl.username, gl.pass);
            if (BuyRB.Checked == true)
            {


                string SomeSuplies = @"", empname = "", comm = "", defect = "";
                float totalcoast = 0;
                string getCoast = "";

                DialogResult result = MessageBox.Show("Сборка " + gl.idCompl + " создана. Распечатать договор?", "Вывод на печать", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    var wordApp = new Word.Application();// создаем новый экземпляр ворда
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Title = "Выберите шаблон";
                    ofd.Filter = "Документы Word |*.doc;*.docx;";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            string employment = "";
                            for (int i = 1; i < dataGridView2.Rows.Count; i++)
                            {
                                MySqlDataReader myReader;
                                string getEmployment = "select supltype from bufers where id =" + i;
                                MySqlCommand getEmp = new MySqlCommand(getEmployment, gl.Connect);
                                myReader = getEmp.ExecuteReader();
                                while (myReader.Read())
                                {
                                    employment = myReader[0].ToString();
                                }
                                myReader.Close();
                                switch (employment)
                                {
                                    case "Cases":
                                        employment = employment.Replace("Cases", "Корпус:");
                                        break;
                                    case "VideoCards":
                                        employment = employment.Replace("VideoCards", "Видеокарта:");
                                        break;
                                    case "Motherboards":
                                        employment = employment.Replace("Motherboards", "Материнская плата:");
                                        break;
                                    case "HDDs":
                                        employment = employment.Replace("HDDs", "Жесткий диск:");
                                        break;
                                    case "DDRs":
                                        employment = employment.Replace("DDRs", "ОЗУ:");
                                        break;
                                    case "SSDs":
                                        employment = employment.Replace("SSDs", "SSD:");
                                        break;
                                    case "Procs":
                                        employment = employment.Replace("Procs", "Процессор:");
                                        break;
                                    case "BPs":
                                        employment = employment.Replace("BPs", "Блок питания:");
                                        break;

                                }
                                string getempname = "select suplname from bufers where id =" + i;
                                MySqlCommand getEmpname = new MySqlCommand(getempname, gl.Connect);
                                myReader = getEmpname.ExecuteReader();
                                while (myReader.Read())
                                {
                                    empname = myReader[0].ToString();
                                }
                                myReader.Close();

                                string getComm = "select commentaries from bufers where id =" + i;
                                MySqlCommand getCom = new MySqlCommand(getComm, gl.Connect);
                                myReader = getCom.ExecuteReader();
                                while (myReader.Read())
                                {
                                    comm = myReader[0].ToString();
                                }
                                myReader.Close();
                                string getDefect = "select defect from bufers where id =" + i;
                                MySqlCommand getdef = new MySqlCommand(getDefect, gl.Connect);
                                myReader = getdef.ExecuteReader();
                                while (myReader.Read())
                                {
                                    defect = myReader[0].ToString();
                                }
                                myReader.Close();

                                SomeSuplies += " " + employment + " " + empname +" (Состояние:"+defect+"% "+comm+");";

                                string getTotalCoast = "select coastwithdefect from bufers where id = " + i;

                                MySqlCommand getcoast = new MySqlCommand(getTotalCoast, gl.Connect);
                                myReader = getcoast.ExecuteReader();
                                while (myReader.Read())
                                {
                                    getCoast = myReader[0].ToString();
                                }
                                myReader.Close();
                                totalcoast += Convert.ToSingle(getCoast);
                            }

                            var wordDoc = wordApp.Documents.Add(ofd.FileName); //Открываем шаблон
                            ReplaceStub("{SomeSuplies}", SomeSuplies, wordDoc);
                            ReplaceStub("{TotalCoast}", Convert.ToString(totalcoast), wordDoc);
                            ReplaceStub("{TotalCoast}", Convert.ToString(totalcoast), wordDoc);
                            wordApp.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Вы не выбрали шаблон", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }




                string ins = "insert into stores(Id_compl,Supl_Id,Supltype,Suplname,Defect,Coastwithdefect,Commentaries) select Id_compl,Supl_Id,Supltype,Suplname,Defect,Coastwithdefect,Commentaries from sellhelper.bufers;";
                MySqlCommand com = new MySqlCommand(ins, gl.Connect);
                com.ExecuteNonQuery();

                string makeahistory = "insert into history(Id_compl,Supl_Id,Supltype,Suplname,Defect,Coastwithdefect,Commentaries,Status) select Id_compl,Supl_Id,Supltype,Suplname,Defect,Coastwithdefect,Commentaries,Status from sellhelper.bufers;";
                MySqlCommand com1 = new MySqlCommand(makeahistory, gl.Connect);
                com1.ExecuteNonQuery();


                string trunc = "truncate bufers;";
                MySqlCommand com2 = new MySqlCommand(trunc, gl.Connect);
                com2.ExecuteNonQuery();

                //  dataGridView2.Rows.Clear();



            }
            else
            {
                string SomeSuplies = "", empname = "";
                float totalcoast = 0;
                string getCoast = "";

                DialogResult result = MessageBox.Show("Сборка " + gl.idCompl + " создана. Распечатать договор?", "Вывод на печать", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    var wordApp = new Word.Application();// создаем новый экземпляр ворда
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Title = "Выберите шаблон";
                    ofd.Filter = "Документы Word |*.doc;*.docx;";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            string employment = "";
                            for (int i = 1; i < dataGridView2.Rows.Count; i++)
                            {
                                MySqlDataReader myReader;
                                string getEmployment = "select supltype from bufers where id =" + i;
                                MySqlCommand getEmp = new MySqlCommand(getEmployment, gl.Connect);
                                myReader = getEmp.ExecuteReader();
                                while (myReader.Read())
                                {
                                    employment = myReader[0].ToString();
                                }
                                myReader.Close();
                                switch (employment)
                                {
                                    case "Cases":
                                        employment = employment.Replace("Cases", "Корпус: ");
                                        break;
                                    case "VideoCards":
                                        employment = employment.Replace("VideoCards", "Видеокарта: ");
                                        break;
                                    case "Motherboards":
                                        employment = employment.Replace("Motherboards", "Материнская плата: ");
                                        break;
                                    case "HDDs":
                                        employment = employment.Replace("HDDs", "Жесткий диск: ");
                                        break;
                                    case "DDRs":
                                        employment = employment.Replace("DDRs", "ОЗУ: ");
                                        break;
                                    case "SSDs":
                                        employment = employment.Replace("SSDs", "SSD: ");
                                        break;
                                    case "Procs":
                                        employment = employment.Replace("Procs", "Процессор: ");
                                        break;
                                    case "BPs":
                                        employment = employment.Replace("BPs", "Блок питания: ");
                                        break;

                                }
                                string getempname = "select suplname from bufers where id =" + i;
                                MySqlCommand getEmpname = new MySqlCommand(getempname, gl.Connect);
                                myReader = getEmpname.ExecuteReader();
                                while (myReader.Read())
                                {
                                    empname = myReader[0].ToString();
                                }
                                myReader.Close();
                                SomeSuplies += " "+employment +" "+ empname+";";

                                string getTotalCoast = "select coastwithdefect from bufers where id = " + i;

                                MySqlCommand getcoast = new MySqlCommand(getTotalCoast, gl.Connect);
                                myReader = getcoast.ExecuteReader();
                                while (myReader.Read())
                                {
                                    getCoast = myReader[0].ToString();
                                }
                                myReader.Close();
                                totalcoast += Convert.ToSingle(getCoast);
                            }

                            var wordDoc = wordApp.Documents.Add(ofd.FileName); //Открываем шаблон
                            ReplaceStub("{SomeSuplies}", SomeSuplies, wordDoc);
                            ReplaceStub("{TotalCoast}", Convert.ToString(totalcoast), wordDoc);
                            wordApp.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Вы не выбрали шаблон", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }
                // удаление из таблицы stores
                string dropquery = " delete from stores where id in (select id_stores from bufers); ";
                MySqlCommand com = new MySqlCommand(dropquery, gl.Connect);
                com.ExecuteNonQuery();

                string makeahistory = "insert into history(Id_compl,Supl_Id,Supltype,Suplname,Defect,Coastwithdefect,Commentaries,Status) select Id_compl,Supl_Id,Supltype,Suplname,Defect,Coastwithdefect,Commentaries,Status from sellhelper.bufers;";
                MySqlCommand com1 = new MySqlCommand(makeahistory, gl.Connect);
                com1.ExecuteNonQuery();

                //очистка буфера 
                string trunc = "truncate bufers;";
                MySqlCommand com2 = new MySqlCommand(trunc, gl.Connect);
                com2.ExecuteNonQuery();

            //    dataGridView2.Rows.Clear();
            }   
        }

        //Отображение в буфере данных по активации формы
        private void Sellform_Activated(object sender, EventArgs e)
        {
            connections.server(gl.username, gl.pass);
            adapter1 = new MySqlDataAdapter(@"select id as ID,
                                                 id_compl as  ID_Сборки,
                                                 Supl_id as ID_Оборудования,
                                                 Supltype as Тип_Оборудования,
                                                 Suplname as Наименование,
                                                 Defect as Дефект,
                                                 CoastWithDefect as Цена_с_дефектом,
                                                 Commentaries as Комментарий
                                                 from bufers ", gl.Connect);
            dataSet1 = new DataSet();
            adapter1.Fill(dataSet1);
            dataGridView2.DataSource = dataSet1.Tables[0];
        }

        //Редактирование буфера
        private void button2_Click(object sender, EventArgs e)
        {
            commandBuilder = new MySqlCommandBuilder(adapter1);
            adapter1.Update(dataSet1);
            MessageBox.Show("Изменения в базе данных выполнены!", "Уведомление о результатах", MessageBoxButtons.OK);
        }

        //полуение заголовка столбца и его индекса
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {   
                gl.col = e.ColumnIndex;
                gl.colname = "";
                gl.colname = dataGridView1.Columns[gl.col].HeaderText;
            }
            catch
            {
                gl.col = e.ColumnIndex;
                gl.colname = "";
                gl.colname = dataGridView1.Columns[gl.col+1].HeaderText;
            }
        }

        private void Sellform_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void SellRB_CheckedChanged(object sender, EventArgs e)
        {
            ChoseCB.Text = "Stores";
            updateGrid();
            dataGridView1.ReadOnly = true;
        }
        //Обработка исключения
        private void dataGridView2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Входное значение имело неверный формат", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
